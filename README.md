# docker_ignition_mysql
For using distributed Ignition 8.1 and SQL database using docker technology. The purpose of this Repo is to contain the Docker-compose and files for UNS and MQTT Testing. This Repo is public, but only shared with the Industry 4.0 Community. 

If you find this Repo and its files to be of use, Let me know. 

If you are interested in working on this with me or maybe youre a docker/kubernetes guru who can help me get better?
# Contact me on the Industry 4.0 Discord https://discord.gg/VFSz3wAzyT @ TaylorTurner#7634 


## Project status
I do not currently maintain this repo. It is listed as a static resource for those looking for a better/faster way to develop with docker and use more fficient workflows like get tracking projects.

## Kubernetes
I have included a ignition-deply.yaml that works with k3s, only the ignition gateway is currently accessible.
If youre a devops pro, contact me and i am willing to work to make this more robust. (Data accross SQL and volume-reattachment)

## REQUIREMENTS
- WSL2 (Windows sub linux) and WSL1
    - Pre-requisite for Docker Desktop
- Docker dekstop (Windows) or Docker Enginge must be installed. 
    -verify by running "docker run hello-world " (sudo only if needed)
- With docker engine/desktop running complete either path

# PATH 1
Options:
1. If you want a clean dev environment move to path 2
2. If you want to continue an existing GW Backup
    - Put your .gwbk file into respective folder to be cpoied into the "restore.gwbk" in the container



# PATH 2
## ( Docker Desktop/Engine MUST be running for this step )
1. If using VS Code (you should) with Docker extension installed
    - Open the "docker-compose.yml"
    - In the file editing window, right click and select "Compose Up"
        This deploys all containers in the compose file
    A. It is also possible to right click and "Select Services"
    - This allows you deploy only the containers you check the box
        - An example would be only the central gateway or MySQL Server for less running resource testing.

2. This will build your apps, connect the network you should be ready for some development


## NOTES
1. There are volumes created to retain any changes made.
2. It is recommended to fork this repo, then clone it to your remote (computer).
    - This allows git version tracking of your projects (think multiple users), screens and views.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Contributing
Open to contributions and improvements. Contact me directly, I do not track bug reports on this repo.
But if you have improvements, Fork it. Contact me (Discord) and I will let you know if its approved for merging.

## License
MIT open sourece licensed


